# Life
Life is an implementation of the Conway's Game of Life in Go. 

## Download
```bash
go get gitlab.com/ollybritton/life/...
```